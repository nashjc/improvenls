rm(list=ls())
require(nlsr); require(minpack.lm)
# NOTE: wfct() is taken from the minpack.lm package.
# It can be used in the call to functions, but it may make it
# clearer to build the weights first
# We subset the data to use only cases that are "treated"
Treated <- Puromycin[Puromycin$state == "treated", ]
wts <- 1/Treated$rate
st <- c(Vm = 200, K = 0.05)
jm<-model2rjfun(rate ~ Vm * conc/(K + conc), data = Treated, pvec = st)

wtt1nlx<-nlxb(rate ~ Vm * conc/(K + conc), data = Treated, trace=TRUE,
              start = st, weights = wts)
print(wtt1nlx)
#?? psi and phi seem to make no difference.
# wtt1nlxa<-nlxb22(rate ~ Vm * conc/(K + conc), data = Treated, start = st, 
#                   weights = wts, trace=TRUE)
# print(wtt1nlxa)
## In the following note that the parameter estimates do NOT change
## but the standard errors and t-stats do.
wtt2nlx<-nlxb(rate ~ Vm * conc/(K + conc), data = Treated,
       start = c(Vm = 200, K = 0.05), weights = wfct(sqrt(conc)))
print(wtt2nlx)

# check nls
wtt1nls<-nls(rate ~ Vm * conc/(K + conc), data = Treated, trace=TRUE,
              start = c(Vm = 200, K = 0.05), weights = wfct(1/rate))
summary(wtt1nls)
wtt1nlsa<-nls(rate ~ Vm * conc/(K + conc), data = Treated,
             start = c(Vm = 200, K = 0.05), weights = wts)
summary(wtt1nlsa)
## In the following note that the parameter estimates do NOT change
## but the standard errors and t-stats do.
wtt2nls<-nls(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(sqrt(conc)))
summary(wtt2nls)

# minpack.lm
wtt1nlsLM<-nlsLM(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(1/rate))
summary(wtt1nlsLM)
wtt2nlsLM<-nlsLM(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(sqrt(conc)))
summary(wtt2nlsLM)

# These don't work -- there is no fitted() function returned by nlsr
# wtt3nlx<-try(nlxb(rate ~ Vm * conc/(K + conc), data = Treated,
#        start = c(Vm = 200, K = 0.05), weights = wfct(1/fitted^2)))
##  (nlxb(rate ~ Vm * conc/(K + conc), data = Treated,
##        start = c(Vm = 200, K = 0.05), weights = wfct(1/(resid+rate)^2)))
## (wrapnlsr(rate ~ Vm * conc/(K + conc), data = Treated,
##        start = c(Vm = 200, K = 0.05), weights = wfct(1/(resid+rate)^2)))
## wtt5nlx<-nlxb(rate ~ Vm * conc/(K + conc), data = Treated,
#     start = c(Vm = 200, K = 0.05), weights = wfct(1/resid^2))
## print(wtt5nlx)

wtt4nlx<-nlxb(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(1/error^2))
print(wtt4nlx)
wtt4nlm<-nlsLM(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(1/error^2))
print(wtt4nlm)
wtt4nls<-nls(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(1/error^2))
print(wtt4nls)

wtt5nls<-nls(rate ~ Vm * conc/(K + conc), data = Treated,
              start = c(Vm = 200, K = 0.05), weights = wfct(1/resid^2))
print(wtt5nls)

## weighted nonlinear regression
Treated <- Puromycin[Puromycin$state == "treated", ]
weighted.MM <- function(resp, conc, Vm, K)
{
    ## Purpose: exactly as white book p. 451 -- RHS for nls()
    ##  Weighted version of Michaelis-Menten model
    ## ----------------------------------------------------------
    ## Arguments: 'y', 'x' and the two parameters (see book)
    ## ----------------------------------------------------------
    ## Author: Martin Maechler, Date: 23 Mar 2001

    pred <- (Vm * conc)/(K + conc)
    (resp - pred) / sqrt(pred)
}

Pur.wtj <- nlsj( ~ weighted.MM(rate, conc, Vm, K), data = Treated,
              start = list(Vm = 200, K = 0.1))
summary(Pur.wtj)

## Passing arguments using a list that can not be coerced to a data.frame
lisTreat <- with(Treated,
                 list(conc1 = conc[1], conc.1 = conc[-1], rate = rate))

weighted.MM1 <- function(resp, conc1, conc.1, Vm, K)
{
     conc <- c(conc1, conc.1)
     pred <- (Vm * conc)/(K + conc)
    (resp - pred) / sqrt(pred)
}
Pur.wt1j <- nlsj( ~ weighted.MM1(rate, conc1, conc.1, Vm, K),
               data = lisTreat, start = list(Vm = 200, K = 0.1))
stopifnot(all.equal(coef(Pur.wtj), coef(Pur.wt1j)))

## Chambers and Hastie (1992) Statistical Models in S  (p. 537):
## If the value of the right side [of formula] has an attribute called
## 'gradient' this should be a matrix with the number of rows equal
## to the length of the response and one column for each parameter.

weighted.MM.grad <- function(resp, conc1, conc.1, Vm, K)
{
  conc <- c(conc1, conc.1)

  K.conc <- K+conc
  dy.dV <- conc/K.conc
  dy.dK <- -Vm*dy.dV/K.conc
  pred <- Vm*dy.dV
  pred.5 <- sqrt(pred)
  dev <- (resp - pred) / pred.5
  Ddev <- -0.5*(resp+pred)/(pred.5*pred)
  attr(dev, "gradient") <- Ddev * cbind(Vm = dy.dV, K = dy.dK)
  dev
}

Pur.wt.gradj <- nlsj( ~ weighted.MM.grad(rate, conc1, conc.1, Vm, K),
                   data = lisTreat, start = list(Vm = 200, K = 0.1))

rbind(coef(Pur.wtj), coef(Pur.wt1j), coef(Pur.wt.gradj))

## In this example, there seems no advantage to providing the gradient.
## In other cases, there might be.

