# hobbsunc.R -- hobbs formula approach with timing
rm(list=ls())
library(nlsr)
library(minpack.lm)
library(microbenchmark)
wu <- 2
# hobbsdata.R
weed <- c(5.308, 7.24, 9.638, 12.866, 17.069, 23.192, 31.443,
          38.558, 50.156, 62.948, 75.995, 91.972)
tt <- 1:12
weeddf <- data.frame(tt, weed)
wmodu <- weed ~ b1/(1+b2*exp(-b3*tt))
st2c <- "c(b1=200, b2=50, b3=0.3)"
st2<-eval(parse(text=st2c))
cat("nls from unit start\n")
hunls1 <- try(nls(wmodu, data=weeddf, start=c(b1=1, b2=1, b3=1), trace=FALSE))
if (! inherits(hunls1, "try-error")) print(hunls1) ## else cat("Failure -- try-error\n")
cat("nlxb from unit start\n")
hunlx1 <- try(nlxb(wmodu, data=weeddf, start=c(b1=1, b2=1, b3=1), trace=FALSE))
if (! inherits(hunlx1, "try-error")) print(hunlx1)
# pctrl(nlsr.control())
tnls<-microbenchmark(hunls2 <- try(nls(wmodu, data=weeddf, start=st2, trace=FALSE)), control=list(warmup=wu))
# cat("tnls\n")
# print(tnls)
cat("nls result from good start\n")
pnls0(hunls2)
# cat("tnlxbdef\n")
tnlxbdef<-microbenchmark(hunlxb2 <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE)), control=list(warmup=wu))
cat("nlxb result from good start\n")
pshort(hunlxb2)
# cat("tnlxb11\n")
tnlxb11<-microbenchmark(hunlxb2 <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(phi=1, psi=1))), control=list(warmup=wu))
# print(tnlxb11)
# cat("tnlxb01\n")
tnlxb01<-microbenchmark(hunlxb2 <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(phi=1, psi=0))), control=list(warmup=wu))
# print(tnlxb01)
# cat("tnlxb10\n")
tnlxb10<-microbenchmark(hunlxb2 <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(phi=0, psi=1))), control=list(warmup=wu))
# print(tnlxb10)

# does not work
# hunlxb2nuder <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(japprox="januder")))
# hunlxb2nuder
# st2<-c(b1=1, b2=1, b3=1)
tnlxbfwd<-microbenchmark(hunlxb2fwd <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(japprox="jafwd"))),control=list(warmup=wu))
# hunlxb2fwd
tnlxbback<-microbenchmark(hunlxb2back <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(japprox="jaback"))),control=list(warmup=wu))
# hunlxb2back
tnlxbcentral<-microbenchmark(hunlxb2central <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(japprox="jacentral"))),control=list(warmup=wu))
# hunlxb2central
tnlxbnd<-microbenchmark(hunlxb2nd <-  try(nlxb(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(japprox="jand"))),control=list(warmup=wu))
# hunlxb2nd
# NOTE: no warning below that japprox is NOT a control for nlsLM
tnlsLM<-microbenchmark(hunlsLM2nd <-  try(nlsLM(wmodu, data=weeddf, start=st2, trace=FALSE, control=list(japprox="jand"))),control=list(warmup=wu))
# hunlsLM2nd

ttnls<-tnls$time*1e-6 # time stored as nanosecs, unit is used for display only
ttnlxb10<-tnlxb10$time*1e-6
ttnlxb11<-tnlxb11$time*1e-6
ttnlxb01<-tnlxb01$time*1e-6
ttnlxbdef<-tnlxbdef$time*1e-6
ttnlxbfwd<-tnlxbfwd$time*1e-6
ttnlxbback<-tnlxbback$time*1e-6
ttnlxbcentral<-tnlxbcentral$time*1e-6
ttnlxbnd<-tnlxbnd$time*1e-6
ttnlsLM<-tnlsLM$time*1e-6
bpdef<-data.frame(ttnls, ttnlxbdef, ttnlxb11, ttnlxb10, ttnlxb01, ttnlxbfwd, ttnlxbback,
                   ttnlxbcentral, ttnlxbnd, ttnlsLM)
colnames(bpdef)<-c("nls", "xdef", "x11", "x10", "x01", "xf", "xb","xc", "xnd", "LM")
mtitl<-paste("msecs for wmodu problem", st2c)
boxplot(bpdef, main=mtitl)
