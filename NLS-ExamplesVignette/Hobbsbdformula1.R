# Hobbsbdformula1.R -- bounds with formula specification of problem
rm(list=ls()) # clear workspace for each major section
library(nlsr)
require(minpack.lm)
## Use the Hobbs Weed problem
weed <- c(5.308, 7.24, 9.638, 12.866, 17.069, 23.192, 31.443,
          38.558, 50.156, 62.948, 75.995, 91.972)
tt <- 1:12
weeddf <- data.frame(y=weed, tt=tt)
## Scaled model
wmods <- y ~ 100*b1/(1+10*b2*exp(-0.1*b3*tt))
traceval<-FALSE
# feasible start i.e. on or within bounds
start1 <- c(b1=1, b2=1, b3=1) # a default starting vector (named!)
nvec(start1)
# Hobbs scaled problem with bounds, formula specification
cat("anlshob1b <- nls(wmods, start=start1, trace=traceval, data=weeddf, lower=c(0,0,0),
             upper=c(2,6,3), algorithm='port')\n")
anlshob1b <- nls(wmods, start=start1, trace=traceval, data=weeddf, lower=c(0,0,0),
             upper=c(2,6,3), algorithm='port')
pnls0(anlshob1b) #  check the answer
cat("nlsLM seems NOT to work with bounds in this example\n")
cat("anlsLM1b <- nlsLM(wmods, start=start1, data=weeddf, lower=c(0,0,0),
                 upper=c(2,6,3), trace=traceval)\n")
anlsLM1b <- nlsLM(wmods, start=start1, data=weeddf, lower=c(0,0,0),
                 upper=c(2,6,3), trace=traceval)
pnls0(anlsLM1b)
# also no warning if starting out of bounds, but gets good answer!!
st4<-c(b1=4, b2=4, b3=4)
anlsLMob <- nlsLM(wmods, start=st4, data=weeddf, lower=c(0,0,0),
                 upper=c(2,6,3), trace=TRUE)
pnls0(anlsLMob)

cat("anlx1b <- nlxb(wmods, start=start1, data=weeddf, lower=c(0,0,0),
                  upper=c(2,6,3), trace=traceval)\n")
anlx1b <- nlxb(wmods, start=start1, data=weeddf, lower=c(0,0,0),
                  upper=c(2,6,3), trace=traceval)
pshort(anlx1b)
