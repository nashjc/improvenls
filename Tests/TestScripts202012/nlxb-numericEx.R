rm(list=ls())
require(nlsr)
source("xnlxb.R")
# source("xnlfb.R")

traceval  <-  TRUE  # traceval set TRUE to debug or give full history

# Data for Hobbs problem
ydat  <-  c(5.308, 7.24, 9.638, 12.866, 17.069, 23.192, 31.443, 
          38.558, 50.156, 62.948, 75.995, 91.972) # for testing
tdat  <-  seq_along(ydat) # for testing

# A simple starting vector -- must have named parameters for nlxb, nls, wrapnlsr.
start1  <-  c(b1=1, b2=1, b3=1)
startf1  <-  c(b1=1, b2=1, b3=.1)

eunsc  <-   y ~ b1/(1+b2*exp(-b3*tt))

cat("LOCAL DATA IN DATA FRAMES\n")
weeddata1  <-  data.frame(y=ydat, tt=tdat)
weeddata2  <-  data.frame(y=1.5*ydat, tt=tdat)

anlxb1  <-  try(nlxb(eunsc, start=start1, trace=traceval, data=weeddata1))
print(anlxb1)
st1list<-list(b1=1, b2=1, b3=1)
anlxb1list  <-  try(nlxb(eunsc, start=st1list, trace=traceval, data=weeddata1))
print(anlxb1list)

anlxb1fwd  <-  (nlxb(eunsc, start=start1, trace=traceval, data=weeddata1, 
    control=list(japprox="jafwd")))
print(anlxb1fwd)

cat("fwd - analytic\n")
print(anlxb1fwd$coefficients - anlxb1$coefficients)

anlxb1bak  <-  (nlxb(eunsc, start=start1, trace=traceval, data=weeddata1, 
                     control=list(japprox="jaback")))
print(anlxb1bak)
cat("bak - analytic\n")
print(anlxb1bak$coefficients - anlxb1$coefficients)

anlxb1cen  <-  (nlxb(eunsc, start=start1, trace=traceval, data=weeddata1, 
                     control=list(japprox="jacentral")))
print(anlxb1cen)
cat("central - analytic\n")
print(anlxb1cen$coefficients - anlxb1$coefficients)


anlxb1nd  <-  (nlxb(eunsc, start=start1, trace=traceval, data=weeddata1, 
                     control=list(japprox="jand")))
print(anlxb1nd)
cat("numDeriv - analytic\n")
print(anlxb1nd$coefficients - anlxb1$coefficients)


