
R version 4.2.2 Patched (2022-11-10 r83330) -- "Innocent and Trusting"
Copyright (C) 2022 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> pkgname <- "usl"
> source(file.path(R.home("share"), "R", "examples-header.R"))
> options(warn = 1)
> base::assign(".ExTimings", "usl-Ex.timings", pos = 'CheckExEnv')
> base::cat("name\tuser\tsystem\telapsed\n", file=base::get(".ExTimings", pos = 'CheckExEnv'))
> base::assign(".format_ptime",
+ function(x) {
+   if(!is.na(x[4L])) x[1L] <- x[1L] + x[4L]
+   if(!is.na(x[5L])) x[2L] <- x[2L] + x[5L]
+   options(OutDec = '.')
+   format(x[1L:3L], digits = 7L)
+ },
+ pos = 'CheckExEnv')
> 
> ### * </HEADER>
> library('usl')
> 
> base::assign(".oldSearch", base::search(), pos = 'CheckExEnv')
> base::assign(".old_wd", base::getwd(), pos = 'CheckExEnv')
> cleanEx()
> nameEx("cash-USL-method")
> ### * cash-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: $,USL-method
> ### Title: Extract parts of a "'USL'" object
> ### Aliases: $,USL-method
> ### Keywords: internal
> 
> ### ** Examples
> 
> ## Not run: 
> ##D ## get coefficients from a usl model
> ##D usl.model$coefficients
> ## End(Not run)
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("cash-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("confint-USL-method")
> ### * confint-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: confint,USL-method
> ### Title: Confidence Intervals for USL model parameters
> ### Aliases: confint,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(specsdm91)
> 
> ## Create USL model
> usl.model <- usl(throughput ~ load, specsdm91)
> 
> ## Print confidence intervals
> confint(usl.model)
             2.5 %       97.5 %
alpha 1.000331e-02 4.545364e-02
beta  6.574425e-05 1.429867e-04
gamma 6.237586e+01 1.176146e+02
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("confint-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("efficiency-USL-method")
> ### * efficiency-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: efficiency,USL-method
> ### Title: Efficiency of the system
> ### Aliases: efficiency,USL-method efficiency
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Show the efficiency
> efficiency(usl(throughput ~ processors, raytracer))
        1         4         8        12        16        20        24        28 
0.9153803 0.8924958 0.7437465 0.6483944 0.5435070 0.4576901 0.4004789 0.3759598 
       32        48        64 
0.3718732 0.2669859 0.2216937 
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("efficiency-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("limit.scalability-USL-method")
> ### * limit.scalability-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: limit.scalability,USL-method
> ### Title: Scalability limit of a USL model
> ### Aliases: limit.scalability,USL-method limit.scalability
> 
> ### ** Examples
> 
> require(usl)
> 
> data(specsdm91)
> 
> limit.scalability(usl(throughput ~ load, specsdm91))
[1] 3245.589
> ## The throughput limit is about 3245
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("limit.scalability-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("optimal.scalability-USL-method")
> ### * optimal.scalability-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: optimal.scalability,USL-method
> ### Title: Point of optimal scalability of a USL model
> ### Aliases: optimal.scalability,USL-method optimal.scalability
> 
> ### ** Examples
> 
> require(usl)
> 
> data(specsdm91)
> 
> optimal.scalability(usl(throughput ~ load, specsdm91))
[1] 36.06401
> ## Optimal scalability will be reached at about 36 virtual users
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("optimal.scalability-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("overhead-USL-method")
> ### * overhead-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: overhead,USL-method
> ### Title: Overhead method for Universal Scalability Law models
> ### Aliases: overhead,USL-method overhead
> 
> ### ** Examples
> 
> require(usl)
> 
> data(specsdm91)
> 
> ## Print overhead in processing time for demo dataset
> overhead(usl(throughput ~ load, specsdm91))
        ideal contention    coherency
1 1.000000000 0.00000000 0.0000000000
2 0.055555556 0.02618800 0.0008871066
3 0.027777778 0.02695824 0.0018263959
4 0.013888889 0.02734336 0.0037049746
5 0.009259259 0.02747173 0.0055835533
6 0.006944444 0.02753592 0.0074621319
7 0.004629630 0.02760010 0.0112192893
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("overhead-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("peak.scalability-USL-method")
> ### * peak.scalability-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: peak.scalability,USL-method
> ### Title: Point of peak scalability of a USL model
> ### Aliases: peak.scalability,USL-method peak.scalability
> 
> ### ** Examples
> 
> require(usl)
> 
> data(specsdm91)
> 
> peak.scalability(usl(throughput ~ load, specsdm91))
[1] 96.51956
> ## Peak scalability will be reached at about 96 virtual users
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("peak.scalability-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("plot-USL-method")
> ### * plot-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: plot,USL-method
> ### Title: Plot the scalability function from a USL model
> ### Aliases: plot,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(specsdm91)
> 
> ## Plot result from USL model for demo dataset
> plot(usl(throughput ~ load, specsdm91), bounds = TRUE, ylim = c(0, 3500))
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("plot-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("predict-USL-method")
> ### * predict-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: predict,USL-method
> ### Title: Predict method for Universal Scalability Law models
> ### Aliases: predict,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Print predicted result from USL model for demo dataset
> predict(usl(throughput ~ processors, raytracer))
        1         2         3         4         5         6         7         8 
 21.84884  74.48602 124.45977 160.31155 187.28633 208.31785 225.17539 238.98936 
        9        10        11 
250.51576 282.28276 301.39198 
> 
> ## The same prediction with confidence intervals at the 99% level
> predict(usl(throughput ~ processors, raytracer),
+         interval = "confidence", level = 0.99)
         fit        lwr       upr
1   21.84884  -1.228844  44.92653
2   74.48602  51.408337  97.56371
3  124.45977 101.382088 147.53746
4  160.31155 137.233867 183.38924
5  187.28633 164.208640 210.36401
6  208.31785 185.240161 231.39553
7  225.17539 202.097704 248.25308
8  238.98936 215.911669 262.06704
9  250.51576 227.438069 273.59344
10 282.28276 259.205077 305.36045
11 301.39198 278.314296 324.46967
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("predict-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("print-USL-method")
> ### * print-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: print,USL-method
> ### Title: Print objects of class "'USL'"
> ### Aliases: print,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Print result from USL model for demo dataset
> print(usl(throughput ~ processors, raytracer))

Call:
usl(formula = throughput ~ processors, data = raytracer)

Efficiency:
   Min     1Q Median     3Q    Max 
0.2217 0.3739 0.4577 0.6961 0.9154 

Residuals:
    Min      1Q  Median      3Q     Max 
-15.175  -5.300   2.714   7.074   9.688 

Coefficients:
        Estimate  Std. Error  t value  Pr(>|t|)     
alpha  5.777e-02   1.329e-02    4.346   0.00246  ** 
beta   0.000e+00   1.179e-04    0.000   1.00000     
gamma  2.185e+01   2.196e+00    9.949  8.82e-06  ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 9.336 on 8 degrees of freedom

Scalability bounds:
limit: throughput 378.2 (Amdahl asymptote)
peak:  none (beta=0)
opt:   throughput 194.7 at processors 17.31 

> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("print-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("scalability-USL-method")
> ### * scalability-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: scalability,USL-method
> ### Title: Scalability function of a USL model
> ### Aliases: scalability,USL-method scalability
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Compute the scalability function
> scf <- scalability(usl(throughput ~ processors, raytracer))
> 
> ## Print scalability for 32 CPUs for the demo dataset
> print(scf(32))
[1] 250.5158
> 
> ## Plot scalability for the range from 1 to 64 CPUs
> plot(scf, from=1, to=64)
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("scalability-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("show-USL-method")
> ### * show-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: show,USL-method
> ### Title: Show objects of class "'USL'"
> ### Aliases: show,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Show USL model
> show(usl(throughput ~ processors, raytracer))

Call:
usl(formula = throughput ~ processors, data = raytracer)

Efficiency:
   Min     1Q Median     3Q    Max 
0.2217 0.3739 0.4577 0.6961 0.9154 

Residuals:
    Min      1Q  Median      3Q     Max 
-15.175  -5.300   2.714   7.074   9.688 

Coefficients:
        Estimate  Std. Error  t value  Pr(>|t|)     
alpha  5.777e-02   1.329e-02    4.346   0.00246  ** 
beta   0.000e+00   1.179e-04    0.000   1.00000     
gamma  2.185e+01   2.196e+00    9.949  8.82e-06  ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 9.336 on 8 degrees of freedom

Scalability bounds:
limit: throughput 378.2 (Amdahl asymptote)
peak:  none (beta=0)
opt:   throughput 194.7 at processors 17.31 

> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("show-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("sigma-USL-method")
> ### * sigma-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: sigma,USL-method
> ### Title: Extract Residual Standard Deviation 'Sigma'
> ### Aliases: sigma,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Print result from USL model for demo dataset
> print(sigma(usl(throughput ~ processors, raytracer)))
[1] 9.335669
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("sigma-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("summary-USL-method")
> ### * summary-USL-method
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: summary,USL-method
> ### Title: USL Object Summary
> ### Aliases: summary,USL-method
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Show summary for demo dataset
> summary(usl(throughput ~ processors, raytracer))

Call:
usl(formula = throughput ~ processors, data = raytracer)

Efficiency:
   Min     1Q Median     3Q    Max 
0.2217 0.3739 0.4577 0.6961 0.9154 

Residuals:
    Min      1Q  Median      3Q     Max 
-15.175  -5.300   2.714   7.074   9.688 

Coefficients:
        Estimate  Std. Error  t value  Pr(>|t|)     
alpha  5.777e-02   1.329e-02    4.346   0.00246  ** 
beta   0.000e+00   1.179e-04    0.000   1.00000     
gamma  2.185e+01   2.196e+00    9.949  8.82e-06  ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 9.336 on 8 degrees of freedom

Scalability bounds:
limit: throughput 378.2 (Amdahl asymptote)
peak:  none (beta=0)
opt:   throughput 194.7 at processors 17.31 

> 
> ## Extract model coefficients
> summary(usl(throughput ~ processors, raytracer))$coefficients
      alpha        beta       gamma 
 0.05777078  0.00000000 21.84884283 
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("summary-USL-method", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("usl")
> ### * usl
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: usl
> ### Title: Create a model for the Universal Scalability Law
> ### Aliases: usl
> 
> ### ** Examples
> 
> require(usl)
> 
> data(raytracer)
> 
> ## Create USL model for "throughput" by "processors"
> usl.model <- usl(throughput ~ processors, raytracer)
> 
> ## Show summary of model parameters
> summary(usl.model)

Call:
usl(formula = throughput ~ processors, data = raytracer)

Efficiency:
   Min     1Q Median     3Q    Max 
0.2217 0.3739 0.4577 0.6961 0.9154 

Residuals:
    Min      1Q  Median      3Q     Max 
-15.175  -5.300   2.714   7.074   9.688 

Coefficients:
        Estimate  Std. Error  t value  Pr(>|t|)     
alpha  5.777e-02   1.329e-02    4.346   0.00246  ** 
beta   0.000e+00   1.179e-04    0.000   1.00000     
gamma  2.185e+01   2.196e+00    9.949  8.82e-06  ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 9.336 on 8 degrees of freedom

Scalability bounds:
limit: throughput 378.2 (Amdahl asymptote)
peak:  none (beta=0)
opt:   throughput 194.7 at processors 17.31 

> 
> ## Show complete list of efficiency parameters
> efficiency(usl.model)
        1         4         8        12        16        20        24        28 
0.9153803 0.8924958 0.7437465 0.6483944 0.5435070 0.4576901 0.4004789 0.3759598 
       32        48        64 
0.3718732 0.2669859 0.2216937 
> 
> ## Extract coefficients for model
> coef(usl.model)
      alpha        beta       gamma 
 0.05777078  0.00000000 21.84884283 
> 
> ## Calculate point of peak scalability
> peak.scalability(usl.model)
[1] Inf
> 
> ## Plot original data and scalability function
> plot(raytracer)
> plot(usl.model, add=TRUE)
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("usl", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> ### * <FOOTER>
> ###
> cleanEx()
> options(digits = 7L)
> base::cat("Time elapsed: ", proc.time() - base::get("ptime", pos = 'CheckExEnv'),"\n")
Time elapsed:  0.271 0.017 0.287 0 0 
> grDevices::dev.off()
null device 
          1 
> ###
> ### Local variables: ***
> ### mode: outline-minor ***
> ### outline-regexp: "\\(> \\)?### [*]+" ***
> ### End: ***
> quit('no')
